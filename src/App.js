import './App.css';
import React, { Component } from 'react';

class App extends Component {
  state = {
    index : 0 , 
    classList : [
      {
          "id": 1,
          "price": 30,
          "name": "GUCCI G8850U",
          "url": "./img_source/glassesImage/v1.png",
          "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
      },
      {
          "id": 2,
          "price": 50,
          "name": "GUCCI G8759H",
          "url": "./img_source/glassesImage/v2.png",
          "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
      },
      {
          "id": 3,
          "price": 30,
          "name": "DIOR D6700HQ",
          "url": "./img_source/glassesImage/v3.png",
          "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
      },
      {
          "id": 4,
          "price": 70,
          "name": "DIOR D6005U",
          "url": "./img_source/glassesImage/v4.png",
          "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
      },
      {
          "id": 5,
          "price": 40,
          "name": "PRADA P8750",
          "url": "./img_source/glassesImage/v5.png",
          "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
      },
      {
          "id": 6,
          "price": 60,
          "name": "PRADA P9700",
          "url": "./img_source/glassesImage/v6.png",
          "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
      },
      {
          "id": 7,
          "price": 80,
          "name": "FENDI F8750",
          "url": "./img_source/glassesImage/v7.png",
          "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
      },
      {
          "id": 8,
          "price": 100,
          "name": "FENDI F8500",
          "url": "./img_source/glassesImage/v8.png",
          "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
      },
      {
          "id": 9,
          "price": 60,
          "name": "FENDI F4300",
          "url": "./img_source/glassesImage/v9.png",
          "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
      }
  ]
  }
  handleChangeClass = (id) => {
    var viTri = this.state.classList.findIndex(item => item.id == id);
    this.setState({
      index : viTri 
    })
    
  }
  RenderclassList = () => {
    return this.state.classList.map((item) => {
      return <img onClick={() => {
        this.handleChangeClass(item.id)
      }} className='col-2 mb-4 glass__try' src={item.url} alt="" />
    })
  }
  render() {
    return (
    <div>
      <nav className="navbar navbar-expand-lg bg-light navbar-light p-3 fixed-top">
        <div className="container">
          <a className="navbar-brand" href="#">CyberGlass</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item active">
                <a className="nav-link mr-3" href="#">Home <span className="sr-only">(current)</span></a>
              </li>
              <li className="nav-item">
                <a className="nav-link mr-3" href="#">Contact</a>
              </li>
              <li className="nav-item">
                <a className="nav-link mr-3" href="#">Product</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div className="tryclass">
        <div className="container">
          <h1 className='text-center text-white'>Thử kính</h1>
          <div className="mt-5 row try__glass justify-content-around ">
            <div className="col-4">
              <img className='img-fluid' src="./img_source/glassesImage/model.jpg" alt="" />
            </div>  
            <div className="col-4 glass__first">
              <img className='img-fluid model__img' src='./img_source/glassesImage/model.jpg' alt="" />
              <div className="layout text-center">
                <h3>{this.state.classList[this.state.index].name} {this.state.classList[this.state.index].price}</h3>
                <p>{this.state.classList[this.state.index].desc}</p>
              </div>
              <div className="glass">
                <img className='glass__img' src={this.state.classList[this.state.index].url} alt="" />
              </div>
            </div>
          </div>    
          <div className="row mt-5 bg-light rounded-sm p-3">
            {this.RenderclassList()}
          </div>
        </div>
      </div>
    </div>
    );
  }
}

export default App;
